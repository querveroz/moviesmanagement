package com.crud.movies;

import com.crud.movies.service.MovieService;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MoviesApplicationTests {

	@Autowired
	private MovieService movieService;

	@Test
	void searchMovieIsCorrect() {
		String result = "{\"Title\":\"Jumper\",\"Year\":\"2008\",\"Rated\":\"PG-13\",\"Released\":\"14 Feb 2008\",\"Runtime\":\"88 min\",\"Genre\":\"Action, Adventure, Sci-Fi, Thriller\",\"Director\":\"Doug Liman\",\"Actors\":\"Hayden Christensen, Jamie Bell, Rachel Bilson, Diane Lane\",\"Plot\":\"A teenager with teleportation abilities suddenly finds himself in the middle of an ancient war between those like him and their sworn annihilators.\"}";
		String movie = movieService.getMovieInfo("tt0489099").toString();
		Assert.assertEquals(result, movie);
	}

	@Test
	void searchMovieIsNotCorrect() {
		String result = "{}";
		String movie = movieService.getMovieInfo("tt0489099").toString();
		Assert.assertNotEquals(result, movie);
	}
}
