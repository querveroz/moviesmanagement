package com.crud.movies.controller;

import com.crud.movies.entity.Movie;
import com.crud.movies.entity.MovieResult;
import com.crud.movies.entity.OmdbMovie;
import com.crud.movies.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class MoviesController {

    @Autowired
    private MovieService movieService;

    @GetMapping("/search/{imdbID}")
    public OmdbMovie getMovieInfo(@PathVariable String imdbID) {
        OmdbMovie result = movieService.getMovieInfo(imdbID);
        if(result == null) {
            result = new OmdbMovie();
        }

        return result;
    }

    @GetMapping("/movies")
    public List<MovieResult> getAll() {
        List<MovieResult> result = new ArrayList<>();

        List<Movie> movies = movieService.getAll();
        for(Movie movie : movies) {
            OmdbMovie omdbMovie = movieService.getMovieInfo(movie.getImdbID());
            result.add(new MovieResult(movie, omdbMovie));
        }

        return result;
    }

    @GetMapping("/movies/{id}")
    public MovieResult getMovie(@PathVariable int id) {
        Movie movie = movieService.get(id);
        if(movie == null) {
            movie = new Movie();
        }

        OmdbMovie omdbMovie = movieService.getMovieInfo(movie.getImdbID());
        if(omdbMovie == null) {
            omdbMovie = new OmdbMovie();
        }

        MovieResult result = new MovieResult(movie, omdbMovie);
        return result;
    }

    @PostMapping("/movies")
    public String addMovie(@RequestBody Movie movie) {
        movie.setId(0);
        movieService.save(movie);
        return "Movie data saved";
    }

    @PutMapping("/movies/{id}")
    public String updateMovie(@RequestBody Movie movie, @PathVariable int id) {
        Movie aux = movieService.get(id);
        if(aux == null) {
            return "Movie id not found - " + id;
        }

        System.out.println(aux.toString());
        aux.updateData(movie);
        System.out.println(aux.toString());
        movieService.save(aux);
        return "Movie data updated";
    }

    @DeleteMapping("/movies/{id}")
    public String deleteMovie(@PathVariable int id) {
        Movie movie = movieService.get(id);
        if(movie == null) {
            return "Movie id not found - " + id;
        }

        movieService.delete(id);
        return "Movie data deleted";
    }
}
