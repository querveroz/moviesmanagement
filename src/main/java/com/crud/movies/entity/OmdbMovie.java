package com.crud.movies.entity;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect(fieldVisibility=JsonAutoDetect.Visibility.ANY, getterVisibility=JsonAutoDetect.Visibility.NONE, setterVisibility=JsonAutoDetect.Visibility.NONE, creatorVisibility=JsonAutoDetect.Visibility.NONE)
public class OmdbMovie {

    public OmdbMovie() {
    }

    public OmdbMovie(String title, String year, String rated, String released, String runtime, String genre, String director, String actors, String plot) {
        setTitle(title);
        setYear(year);
        setRated(rated);
        setReleased(released);
        setRuntime(runtime);
        setGenre(genre);
        setDirector(director);
        setActors(actors);
        setPlot(plot);
    }

    private String Title;

    private String Year;

    private String Rated;

    private String Released;

    private String Runtime;

    private String Genre;

    private String Director;

    private String Actors;

    private String Plot;

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getYear() {
        return Year;
    }

    public void setYear(String year) {
        Year = year;
    }

    public String getRated() {
        return Rated;
    }

    public void setRated(String rated) {
        Rated = rated;
    }

    public String getReleased() {
        return Released;
    }

    public void setReleased(String released) {
        Released = released;
    }

    public String getRuntime() {
        return Runtime;
    }

    public void setRuntime(String runtime) {
        Runtime = runtime;
    }

    public String getGenre() {
        return Genre;
    }

    public void setGenre(String genre) {
        Genre = genre;
    }

    public String getDirector() {
        return Director;
    }

    public void setDirector(String director) {
        Director = director;
    }

    public String getActors() {
        return Actors;
    }

    public void setActors(String actors) {
        Actors = actors;
    }

    public String getPlot() {
        return Plot;
    }

    public void setPlot(String plot) {
        Plot = plot;
    }

    @Override
    public String toString() {
        return "{\"Title\":\"" + this.getTitle() + "\",\"Year\":\"" + this.getYear() + "\",\"Rated\":\"" + this.getRated() + "\",\"Released\":\"" + this.getReleased() + "\",\"Runtime\":\"" + this.getRuntime() + "\",\"Genre\":\"" + this.getGenre() + "\",\"Director\":\"" + this.getDirector() + "\",\"Actors\":\"" + this.getActors() + "\",\"Plot\":\"" + this.getPlot() + "\"}";
    }
}
