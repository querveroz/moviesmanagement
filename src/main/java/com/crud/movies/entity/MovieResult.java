package com.crud.movies.entity;

public class MovieResult {

    public MovieResult() {
        this.movie = new Movie();
        this.omdbMovie = new OmdbMovie();
    }

    public MovieResult(Movie movie, OmdbMovie omdbMovie) {
        this.setMovie(movie);
        this.setOmdbMovie(omdbMovie);
    }

    private Movie movie;

    private OmdbMovie omdbMovie;

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public OmdbMovie getOmdbMovie() {
        return omdbMovie;
    }

    public void setOmdbMovie(OmdbMovie omdbMovie) {
        this.omdbMovie = omdbMovie;
    }

    @Override
    public String toString() {
        return "{\"movie\":" + this.getMovie().toString() + ",\"omdbMovie\":" + this.getOmdbMovie().toString() + "}";
    }
}
