package com.crud.movies.entity;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="movie")
public class Movie {

    public Movie() {
    }

    public Movie(int rate, String comments, String imdbID, Date date) {
        this.rate = rate;
        this.comments = comments;
        this.imdbID = imdbID;
        this.date = date;
    }

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @Column(name="rate")
    private int rate;

    @Column(name="comments")
    private String comments;

    @Column(name="imdbID")
    private String imdbID;

    @Column(name="date")
    @CreationTimestamp
    private Date date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getImdbID() {
        return imdbID;
    }

    public void setImdbID(String imdbID) {
        this.imdbID = imdbID;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void updateData(Movie movie) {
        if(movie.getImdbID() != null && !this.imdbID.equals(movie.getImdbID()))
            this.setImdbID(movie.getImdbID());

        if(movie.getRate() > 0 && this.rate != movie.getRate())
            this.setRate(movie.getRate());

        if(movie.getComments() != null && !this.comments.equals(movie.getComments()))
            this.setComments(movie.getComments());
    }

    @Override
    public String toString() {
        return "{\"id\":" + this.getId() + ",\"rate\":" + this.getRate() + ",\"comments\":\"" + this.getComments() + "\",\"imdbID\":\"" + this.getImdbID() + "\",\"date\":\"" + this.getDate() + "\"}";
    }
}
