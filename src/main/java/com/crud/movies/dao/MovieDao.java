package com.crud.movies.dao;

import com.crud.movies.entity.Movie;

import java.util.List;

public interface MovieDao {

    List<Movie> getAll();

    Movie get(int id);

    void save(Movie movie);

    void delete(int id);
}
