package com.crud.movies.service;

import com.crud.movies.entity.Movie;
import com.crud.movies.entity.OmdbMovie;

import java.util.List;

public interface MovieService {

    OmdbMovie getMovieInfo(String imdbID);

    List<Movie> getAll();

    Movie get(int id);

    void save(Movie movie);

    void delete(int id);
}
