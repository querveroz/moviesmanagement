package com.crud.movies.service;

import com.crud.movies.dao.MovieDao;
import com.crud.movies.entity.Movie;
import com.crud.movies.entity.OmdbMovie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class MovieServiceImp implements MovieService {

    @Autowired
    private Environment env;

    @Autowired
    private MovieDao movieDao;

    @Override
    public List<Movie> getAll() {
        List<Movie> movies = movieDao.getAll();
        return movies;
    }

    @Override
    public Movie get(int id) {
        Movie movie = movieDao.get(id);
        return movie;
    }

    @Override
    public void save(Movie movie) {
        movieDao.save(movie);
    }

    @Override
    public void delete(int id) {
        movieDao.delete(id);
    }

    @Override
    public OmdbMovie getMovieInfo(String imdbID) {
        if(imdbID == null || imdbID.length() == 0)
            return new OmdbMovie();

        String url = env.getProperty("url");
        String apikey = env.getProperty("apikey");

        String uri = url + "?apikey=" + apikey + "&i=" + imdbID;
        RestTemplate restTemplate = new RestTemplate();
        OmdbMovie result = restTemplate.getForObject(uri, OmdbMovie.class);
        return result;
    }
}
